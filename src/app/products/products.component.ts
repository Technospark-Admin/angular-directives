import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products = [
    {title: 'abc', desc: 'tjachkvsjbdcvsljkcvbalsj'},
    {title: 'abc', desc: 'tjachkvsjbdcvsljkcvbalsj'},
    {title: 'abc', desc: 'tjachkvsjbdcvsljkcvbalsj'},
    {title: 'abc', desc: 'tjachkvsjbdcvsljkcvbalsj'},
    {title: 'abc', desc: 'tjachkvsjbdcvsljkcvbalsj'},
  ]

  modalStatus = false;

  addTitle = '';
  addDesc = '';
  editingIndex = -1;
  constructor(
  ) { }

  ngOnInit() {
  }

  deleteProduct(i){
    this.products.splice(i,1);
  }

  addProduct(){
    if (this.editingIndex === -1){
      this.products.push({title:this.addTitle,desc: this.addDesc});
      this.addTitle = '';
      this.addDesc = '';
    } else {
      this.products[this.editingIndex].title = this.addTitle;
      this.products[this.editingIndex].desc = this.addDesc; 
      this.editingIndex = -1;
      this.addTitle = '';
      this.addDesc = '';
    }
    this.toggleDialog();
  }

  editProduct(index){
    this.editingIndex = index;
    this.addTitle = this.products[index].title;
    this.addDesc = this.products[index].desc;
    this.toggleDialog();
  }

  toggleDialog(){
    this.modalStatus = !this.modalStatus;
  }
}
